
// ADD USERS Collection
db.users.insertMany([
	{
	"firstName": "Diane",
	"lastName": "Murphy",
	"email": "dmurphy@mail.com",
	"isAdmin": false,
	"isActive": true
	},
	{
	"firstName": "Mary",
	"lastName": "Patterson",
	"email": "mpatterson@mail.com",
	"isAdmin": false,
	"isActive": true
	},
	{
	"firstName": "Jeff",
	"lastName": "Firrelli",
	"email": "jfirrelli@mail.com",
	"isAdmin": false,
	"isActive": true
	},
	{
	"firstName": "Gerard",
	"lastName": "Bondur",
	"email": "gbondur@mail.com",
	"isAdmin": false,
	"isActive": true
	},
	{
	"firstName": "Pamela",
	"lastName": "Castillo",
	"email": "pcastillo@mail.com",
	"isAdmin": true,
	"isActive": false
	},
	{
	"firstName": "George",
	"lastName": "Vanauf",
	"email": "gvanauf@mail.com",
	"isAdmin": true,
	"isActive": true
	}
]);


// ADD COURSES Collection
db.courses.insertMany([
	{
	"name": "Professional Development",
	"price": 10000.00


	},
	{
	"name": "Building Processing",
	"price": 13000.00
	}
]);


// UPDATE COURSE WITH ENROLLEES
db.courses.updateOne(
	{
		"name": "Professional Development"
	},
	{ $set: { 
		"Enrollees": 
		[
			"Murphy", 
			"Firelli"
		]
	}}		
);


// UPDATE COURSE WITH ENROLLEES
db.courses.updateOne(
	{
		"name": "Professional Development"
	},
	{ $set: { 
		"Enrollees": 
		[
        	{"_id": ObjectId("6125b3c37c5299ed2b429b79")},
              
        	{"_id": ObjectId("6125b3c37c5299ed2b429b7b")}
		]
		
	}}		
);

db.courses.updateOne(
	{
		"name": "Business Processing"
	},
	{ $set: { 
		"Enrollees": 
		[
        	{"_id": ObjectId("6125b3c37c5299ed2b429b7c")},
              
        	{"_id": ObjectId("6125b3c37c5299ed2b429b7a")}
		]
		
	}}		
);

// Find NOT ADMIN

db.users.find({"isAdmin": false});
	
// Find ALL USERS

db.users.find()

// find ALL COURSES

db.courses.find();